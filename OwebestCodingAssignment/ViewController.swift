//
//  ViewController.swift
//  OwebestCodingAssignment
//
//  Created by Owebest on 13/05/22.
//
import SDWebImage
import UIKit
class DataModel{
    var item1:String!
    var item2:String!
    var item3:String!
    var item4:String!
    var item5:String!

    init(dict:[String:String]){
        self.item1 = dict["item1"]
        self.item2 = dict["item2"]
        self.item3 = dict["item3"]
        self.item4 = dict["item4"]
        self.item5 = dict["item5"]
    }
}

class ViewController: UIViewController {
    @IBOutlet weak var tblView: UITableView!
    var dataImage = [DataModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        getDataApi()
        // Do any additional setup after loading the view.
    }

    func getDataApi(){
    
        let url = URL(string: "https://mocki.io/v1/d9ba4d6d-d609-4875-a0c7-3cb75ce88778")!

        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data {
                do {
                    // make sure this JSON is in the format we expect
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        // try to read out a string array
                        if let dataArr = json["data"] as? [[String:String]] {
                            self.dataImage = dataArr.map({DataModel(dict: $0)})
                        }

                        DispatchQueue.main.async {
                            self.tblView.reloadData()
                        }
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            } else if let error = error {
                print("HTTP Request Failed \(error)")
            }
        }
        task.resume()

    }

}

//MARK: UITableViewDataSource
extension ViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        view.backgroundColor = .blue
        let label = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.frame.width - 30, height: 30))
        label.text = "Header \(section + 1)"
        label.textColor = .white
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return "Header \(section)"
//    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataImage.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "TblViewCell", for: indexPath) as! TblViewCell
        let data = dataImage[indexPath.section]
        if let item1Url = URL(string: data.item1){
            cell.imgView1.sd_setImage(with: item1Url, completed: nil)
        }
        if let item2Url = URL(string: data.item2){
            cell.imgView2.sd_setImage(with: item2Url, completed: nil)
        }
        if let item3Url = URL(string: data.item3){
            cell.imgView3.sd_setImage(with: item3Url, completed: nil)
        }
        if let item4Url = URL(string: data.item4){
            cell.imgView4.sd_setImage(with: item4Url, completed: nil)
        }
        if let item5Url = URL(string: data.item5){
            cell.imgView5.sd_setImage(with: item5Url, completed: nil)
        }
        return cell
    }
}

class TblViewCell: UITableViewCell{
    @IBOutlet weak var imgView1: UIImageView!
    @IBOutlet weak var imgView2: UIImageView!
    @IBOutlet weak var imgView3: UIImageView!
    @IBOutlet weak var imgView4: UIImageView!
    @IBOutlet weak var imgView5: UIImageView!

}


